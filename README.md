# Wireguard on OpenWrt #

*Will update with system files by 6/10/18, 1PM EST*

## Description ##

Setup and installation of Wireguard with OpenWrt

OpenWrt - OpenWrt ​is a highly extensible ​GNU/​Linux ​distribution for embedded devices ​(typically wireless routers). Unlike many other distributions for these routers, OpenWrt ​is built from the ground up to be a full-featured, easily modifiable operating system for your router. In practice, this means that you can have all the features you need with none of the bloat, powered by a Linux kernel ​that's more recent than most other distributions.

Wireguard - WireGuard is a novel VPN that runs inside the Linux Kernel and utilizes state-of-the-art cryptography. It aims to be faster, simpler, leaner, and more useful than IPSec, while avoiding the massive headache. It intends to be considerably more performant than OpenVPN. WireGuard is designed as a general purpose VPN for running on embedded interfaces and super computers alike, fit for many different circumstances. It runs over UDP, and creates secure point-to-point connections over router or bridged configurations.


## Implementation & System Architecture ##

Our firmware, based on OpenWrt with Wireguard implementation, provides secure connections running on our router in partnership with Gl.iNet. Wireguard was chosen to due to its speed, which is different than the usual, extreme decrease in speed when using other VPN technologies, which is usually a 90% decrease in speed versus not having VPN encryption. Wireguard on the other hand has a *tested* 4x increase from other VPN encryptions, with upwards of 90mbps symetrical speeds with Wireguard running, even over wireless connections.

While the encryption will only be installed on our routers, our antennas will have a metering component (https://bitbucket.org/gatsbylabs/network-monitoring-metering-nlbwmon/src/master/) that will work in conjunction with the router and network monitoring and management software. Data will be collected and stored both locally and at our database for verification, account services, etc.  Currently testing two configurations, one where we are encrypting usage data at the CPE level (using hardware encryption with CPU (Raspberry Pi)) for more security and one without encryption (no extra CPU required) to maximize bandwidth. 

The data collected along with our network management software and our database, will be integrated with the blockchain within smart contract rules, to ensure quality internet service, and all transactions are able to be processed trustlessly, and both through fiat currency and cryptocurrency.

## Sources ##

https://github.com/openwrt

https://github.com/WireGuard
